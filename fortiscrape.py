import click
import os
import re
import requests
from bs4 import BeautifulSoup

FBASE = ("https://www.fortinet.com")
FNGFW = ("/products/next-generation-firewall.html")
DOCS = ("docs")
VERSION = 0.1

def create_dir(fdir):
    current_directory = os.getcwd()
    final_directory = os.path.join(current_directory, DOCS, fdir)
    if not os.path.exists(final_directory):
       os.makedirs(final_directory)
    return final_directory

def scrape_fortigate():
    rfngfw = requests.get(FBASE+FNGFW)
    sfngfw = BeautifulSoup(rfngfw.text, 'html.parser')
    lfngfw = []
    ngfwdir = create_dir("fgt")
    print("\n---------------------------------------------------")
    print("## Starting link scraping for FortiGate at:")
    print(f"## {FBASE}{FNGFW}")
    for link in sfngfw.find_all('a', href=re.compile('data-sheets')):
        lfngfw.append(FBASE+link['href'])
    print("## Found:", len(lfngfw), "data sheets.\n")

    for file in lfngfw:
            if file.find('/'):
                filename = file.rsplit('/', 1)[1]
                print(f"## Downloading: {filename}")
                rf = requests.get(file)
                print(f"## Received: {filename}")
                fullpath = os.path.join(ngfwdir, filename)
                open(fullpath, 'wb').write(rf.content)
                print(f"## Wrote: {filename}\n\n")

@click.command()
@click.option('--product', type=click.Choice(['fortigate']), required=True)
def main(product):
    if product == 'fortigate':
        scrape_fortigate()
    else:
        print("\n## No option passed, exiting.\n")

main()
