FROM python:3-slim
LABEL version="0.1"
LABEL maintainer="David J. Meier (david@meier.io)"
LABEL license="MIT"

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN mkdir /usr/src/app/docs

COPY . .

ENTRYPOINT [ "python", "./fortiscrape.py" ]
CMD [ "--help" ]
