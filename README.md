# FortiScrape README #

So... You wanna grab all of the data sheets for FortiStuff?  Then you're
in the right place!

### General Information ###

+ Version 0.1
  * Initial release, YAY!
  * Python 3.6, anything else: best of luck.
  * Provides scraping data sheets for Fortinet product.
  * Two run-time options: native and Docker.
  * Currently supported products:
    - FortiGate
    - Suggestions for other(s)?

### How do I get this working? ###

There are two options to run FortiScrape: native Python and Docker.

If you are already using Docker it's probably the easier of the two.

Docker instructions:

1. Clone this repo.
2. From the command line switch to the repository directory.
3. Build the Docker image:
  `docker build -t fortiscrape .`
4. Run the Docker image to test:
  `docker container run -it --rm fortiscrape`
5. If the above ran successfully (usage info returned) then:
  `docker container run -it --rm -v $(pwd)/docs:/usr/src/app/docs fortiscrape --product fortigate`
6. All files were downloaded into docs\fgt.
7. \#winning

**WARNING** - It's highly recommended to use venv in whatever format you fancy.
Whether that be: venv, pyenv, pipenv - doesn't really matter.  What matters is
you don't blame me for cluttering your system with stuff.  If you don't know
what you're doing, then use Docker instead.  If you do, read on...

Python instructions:

1. Clone this repo.
2. Use pip to install requirements within your venv:
  `pip install -r requirements.txt`
3. Run the Python code to validate dependencies:
  `python fortiscrape.py`
4. If the above ran successfully (usage info returned) then:
  `python fortiscrape.py --product fortigate`
5. All files were downloaded into docs\fgt.
6. \#winning

### Who made this mess? ###

* David J. Meier (david@meier.io)
* Hit me up on FortiChat or internal email as well my FortiPeeps.
